import 'dart:convert';

import 'package:desafio_framework/databases/database_album.dart';
import 'package:desafio_framework/models/album.dart';
import 'package:http/http.dart' as http;

class AlbumApi {
  /// Busca dados na API e armazena no banco de dados local
  static Future<void> getAlbums() async {
    var albums;
    List<Album> listOfAlbums;

    var url = "https://jsonplaceholder.typicode.com/albums";

    var response = await http.get(Uri.parse(url));

    print("status code :  ${response.statusCode} ");

    if (response.statusCode == 200) {
      albums = json.decode(response.body) as List;
    } else {
      throw Exception;
    }

    listOfAlbums = albums.map<Album>((album) => Album.fromJson(album)).toList();

    // insere Albuns no banco de dados local
    listOfAlbums.forEach((element) {
      DatabaseAlbum.db.insert(element.userId, element.id, element.title);
    });
  }
}
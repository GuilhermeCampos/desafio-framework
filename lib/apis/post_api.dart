import 'dart:convert';

import 'package:desafio_framework/databases/database_post.dart';
import 'package:desafio_framework/models/post.dart';
import 'package:http/http.dart' as http;

class PostApi {
  /// Busca dados na API e armazena no banco de dados local
  static Future<void> getPosts() async {
    var posts;
    List<Post> listOfPosts;

    var url = "https://jsonplaceholder.typicode.com/posts";

    var response = await http.get(Uri.parse(url));

    print("status code :  ${response.statusCode} ");

    if (response.statusCode == 200) {
      posts = json.decode(response.body) as List;
    } else {
      throw Exception;
    }

    listOfPosts = posts.map<Post>((post) => Post.fromJson(post)).toList();

    // insere Postagens no banco de dados local
    listOfPosts.forEach((element) {
      DatabasePost.db
          .insert(element.userId, element.id, element.title, element.body);
    });
  }
}
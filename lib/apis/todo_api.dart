import 'dart:convert';

import 'package:desafio_framework/databases/database_todo.dart';
import 'package:desafio_framework/models/todo.dart';
import 'package:http/http.dart' as http;

class TodoApi {
  /// Busca dados na API e armazena no banco de dados local
  static Future<void> getTodos() async {
    var todos;
    List<Todo> listOfTodos;

    var url = "https://jsonplaceholder.typicode.com/todos";

    var response = await http.get(Uri.parse(url));

    print("status code :  ${response.statusCode} ");

    if (response.statusCode == 200) {
      todos = json.decode(response.body) as List;
    } else {
      throw Exception;
    }

    listOfTodos = todos.map<Todo>((todo) => Todo.fromJson(todo)).toList();

    // insere TO-DOs no banco de dados local
    listOfTodos.forEach((element) {
      DatabaseTodo.db
          .insert(element.userId, element.id, element.title, element.completed);
    });
  }
}
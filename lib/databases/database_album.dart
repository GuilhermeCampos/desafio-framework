import 'package:desafio_framework/databases/database_create.dart';
import 'package:desafio_framework/models/album.dart';

class DatabaseAlbum {
  static const String TABLE_ALBUMS = "albums";
  static const String COLUMN_USERID = "userId";
  static const String COLUMN_ID = "id";
  static const String COLUMN_TITLE = "title";

  DatabaseAlbum._();

  static final DatabaseAlbum db = DatabaseAlbum._();

  Future<List<Album>> getAlbums() async {
    final db = await DatabaseProvider.db.database;

    var albums = await db
        .query(TABLE_ALBUMS,
        columns: [COLUMN_USERID, COLUMN_ID, COLUMN_TITLE]);

    List<Album> albumList = [];

    albums.forEach((element) {
      Album album = Album.fromJson(element);

      albumList.add(album);
    });

    return albumList;
  }

  Future<Map<String, dynamic>> insert(int userId, int id, String title) async {
    Map<String, dynamic> row = {
      DatabaseAlbum.COLUMN_USERID : userId,
      DatabaseAlbum.COLUMN_ID : id,
      DatabaseAlbum.COLUMN_TITLE : title
    };
    final db = await DatabaseProvider.db.database;
    await db.insert(TABLE_ALBUMS, row);
    return row;
  }
}

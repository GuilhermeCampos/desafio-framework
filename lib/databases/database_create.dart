import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DatabaseProvider {
  static const String TABLE_POSTS = "posts";
  static const String COLUMN_USERID = "userId";
  static const String COLUMN_ID = "id";
  static const String COLUMN_TITLE = "title";
  static const String COLUMN_BODY = "body";

  static const String TABLE_ALBUMS = "albums";

  static const String TABLE_TODOS = "todos";
  static const String COLUMN_COMPLETED = "completed";


  DatabaseProvider._();

  static final DatabaseProvider db = DatabaseProvider._();

  Database _database;

  Future<Database> get database async {
    print("database getter called");

    if (_database != null) {
      return _database;
    }

    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();

    return await openDatabase(
      join(dbPath, 'desafio_framework.db'),
      version: 1,
      onCreate: (Database database, int version) async {
        print("Creating database tables");

        await database.execute(
          "CREATE TABLE $TABLE_POSTS ("
              "$COLUMN_USERID INTEGER,"
              "$COLUMN_ID INTEGER PRIMARY KEY,"
              "$COLUMN_TITLE TEXT,"
              "$COLUMN_BODY TEXT"
              ")",
        );

        await database.execute(
          "CREATE TABLE $TABLE_ALBUMS ("
              "$COLUMN_USERID INTEGER,"
              "$COLUMN_ID INTEGER PRIMARY KEY,"
              "$COLUMN_TITLE TEXT"
              ")",
        );

        await database.execute(
          "CREATE TABLE $TABLE_TODOS ("
              "$COLUMN_USERID INTEGER,"
              "$COLUMN_ID INTEGER PRIMARY KEY,"
              "$COLUMN_TITLE TEXT,"
              "$COLUMN_COMPLETED INTEGER"
              ")",
        );
      },
    );
  }


}


import 'package:desafio_framework/databases/database_create.dart';
import 'package:desafio_framework/models/post.dart';

class DatabasePost {
  static const String TABLE_POSTS = "posts";
  static const String COLUMN_USERID = "userId";
  static const String COLUMN_ID = "id";
  static const String COLUMN_TITLE = "title";
  static const String COLUMN_BODY = "body";

  DatabasePost._();

  static final DatabasePost db = DatabasePost._();

  Future<List<Post>> getPosts() async {
    final db = await DatabaseProvider.db.database;

    var posts = await db
        .query(TABLE_POSTS,
        columns: [COLUMN_USERID, COLUMN_ID, COLUMN_TITLE, COLUMN_BODY]);

    List<Post> postList = [];

    posts.forEach((element) {
      Post post = Post.fromJson(element);

      postList.add(post);
    });

    return postList;
  }

  Future<Map<String, dynamic>> insert(int userId, int id, String title, String body) async {
    Map<String, dynamic> row = {
      DatabasePost.COLUMN_USERID : userId,
      DatabasePost.COLUMN_ID : id,
      DatabasePost.COLUMN_TITLE : title,
      DatabasePost.COLUMN_BODY : body
    };
    final db = await DatabaseProvider.db.database;
    await db.insert(TABLE_POSTS, row);
    return row;
  }
}

import 'package:desafio_framework/databases/database_create.dart';
import 'package:desafio_framework/models/todo.dart';

class DatabaseTodo {
  static const String TABLE_TODOS = "todos";
  static const String COLUMN_USERID = "userId";
  static const String COLUMN_ID = "id";
  static const String COLUMN_TITLE = "title";
  static const String COLUMN_COMPLETED = "completed";

  DatabaseTodo._();

  static final DatabaseTodo db = DatabaseTodo._();

  Future<List<Todo>> getTodos() async {
    final db = await DatabaseProvider.db.database;

    var todos = await db
        .query(TABLE_TODOS,
        columns: [COLUMN_USERID, COLUMN_ID, COLUMN_TITLE, COLUMN_COMPLETED]);

    List<Todo> todoList = [];

    todos.forEach((element) {

      Todo todo = Todo.fromJson(element);

      todoList.add(todo);
    });

    return todoList;
  }

  Future<Map<String, dynamic>> insert(int userId, int id, String title, bool completed) async {
    // completed teve que ser int porque sqlite não suporta o tipo boolean
    Map<String, dynamic> row = {
      DatabaseTodo.COLUMN_USERID : userId,
      DatabaseTodo.COLUMN_ID : id,
      DatabaseTodo.COLUMN_TITLE : title,
      DatabaseTodo.COLUMN_COMPLETED : completed ? 1 : 0
    };
    final db = await DatabaseProvider.db.database;
    await db.insert(TABLE_TODOS, row);
    return row;
  }
}

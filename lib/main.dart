import 'package:desafio_framework/pages/splash_page.dart';
import 'package:desafio_framework/values/theme_dark.dart';
import 'package:desafio_framework/values/theme_light.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Desafio Framework',
      home: SplashPage(),
      debugShowCheckedModeBanner: false,
      theme: themeLight,
      darkTheme: themeDark,
    );
  }
}

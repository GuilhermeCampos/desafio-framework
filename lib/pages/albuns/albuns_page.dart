import 'package:desafio_framework/databases/database_album.dart';
import 'package:desafio_framework/models/album.dart';
import 'package:desafio_framework/pages/albuns/components/album_tile.dart';
import 'package:flutter/material.dart';

class AlbunsPage extends StatefulWidget {
  @override
  _AlbunsPageState createState() => _AlbunsPageState();
}

class _AlbunsPageState extends State<AlbunsPage> {
  List<Album> albuns = [];
  bool isLoading = true;

  @override
  void initState() {
    loadingAlbuns();
    super.initState();
  }

  /// Carrega os albuns no banco de dados local
  void loadingAlbuns() async {
    List<Album> albums = await DatabaseAlbum.db.getAlbums();
    setState(() {
      albuns = albums;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.black26,
      ),
      itemCount: albuns.length,
      itemBuilder: (context, index) => AlbumTile(
        album: albuns[index],
      ),

    );
  }
}

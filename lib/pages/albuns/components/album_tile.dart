import 'package:desafio_framework/models/album.dart';
import 'package:flutter/material.dart';
import 'package:desafio_framework/values/colors.dart';

class AlbumTile extends StatelessWidget {
  final Album album;

  const AlbumTile({Key key, this.album}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Álbum do usuário: ${album.userId}',
            style: TextStyle(
              fontSize: 12,
              color: (Theme.of(context).brightness == Brightness.light) ? textColorLight : textColorDark,
            ),
          ),
          SizedBox(
            height: 4.0,
          ),
          Text(
            '${album.title}',
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: (Theme.of(context).brightness == Brightness.light) ? titleColorLight : titleColorDark,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:desafio_framework/pages/albuns/albuns_page.dart';
import 'package:desafio_framework/pages/postagens/postagens_page.dart';
import 'package:desafio_framework/pages/todos/todos_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;

  List<Widget> pageTitles = [
    Text('Postagens'),
    Text('Álbuns'),
    Text('TO-DOs')
  ];

  final tabs = [
    PostagensPage(),
    AlbunsPage(),
    TodosPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: pageTitles[_currentIndex],
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.post_add_outlined),
            label: 'Postagens',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.album_outlined),
            label: 'Álbuns',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.done),
            label: 'TO-DOs',
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}

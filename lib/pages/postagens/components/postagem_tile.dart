import 'package:desafio_framework/models/post.dart';
import 'package:desafio_framework/values/colors.dart';
import 'package:flutter/material.dart';

class PostagemTile extends StatelessWidget {
  final Post post;

  const PostagemTile({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Postagem do usuário: ${post.userId}',
            style: TextStyle(
              fontSize: 12,
              color: (Theme.of(context).brightness == Brightness.light)
                  ? textColorLight
                  : textColorDark,
            ),
          ),
          SizedBox(
            height: 4.0,
          ),
          Text(
            '${post.title}',
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: (Theme.of(context).brightness == Brightness.light)
                  ? titleColorLight
                  : titleColorDark,
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          Text(
            '${post.body}',
            style: TextStyle(
              fontSize: 14,
              color: (Theme.of(context).brightness == Brightness.light)
                  ? textColorLight
                  : textColorDark,
            ),
          ),
        ],
      ),
    );
  }
}

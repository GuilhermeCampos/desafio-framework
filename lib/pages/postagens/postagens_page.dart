import 'package:desafio_framework/databases/database_post.dart';
import 'package:desafio_framework/models/post.dart';
import 'package:desafio_framework/pages/postagens/components/postagem_tile.dart';
import 'package:flutter/material.dart';

class PostagensPage extends StatefulWidget {
  @override
  _PostagensPageState createState() => _PostagensPageState();
}

class _PostagensPageState extends State<PostagensPage> {
  List<Post> postagens = [];
  bool isLoading = true;

  @override
  void initState() {
    loadingPosts();
    super.initState();
  }

  /// Carrega as postagens no banco de dados local
  void loadingPosts() async {
    List<Post> posts = await DatabasePost.db.getPosts();
    setState(() {
      postagens = posts;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black26,
            ),
            itemCount: postagens.length,
            itemBuilder: (context, index) => PostagemTile(
              post: postagens[index],
            ),

          );
  }
}

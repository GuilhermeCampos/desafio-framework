import 'dart:async';

import 'package:desafio_framework/apis/album_api.dart';
import 'package:desafio_framework/apis/post_api.dart';
import 'package:desafio_framework/apis/todo_api.dart';
import 'package:desafio_framework/pages/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  @override
  void initState() {

    // verifica a permissão e se estiver garantida sincroniza os dados com a API
    askPermission().then((value) {
      if(value.isGranted) {
        PostApi.getPosts();
        AlbumApi.getAlbums();
        TodoApi.getTodos();
      } else {
        print('Permissão negada');
      }
      delayToHomePage();
    });



    super.initState();
  }

  /// Verifica a permissão de armazenamento
  Future<PermissionStatus> askPermission() async {
    final status = await Permission.storage.request();
    return status;
  }


  delayToHomePage() async {
    var _duration = new Duration(
        seconds: 3);
    return new Timer(_duration, () async {
      navigationToHomePage();
      return;
    });
  }

  void navigationToHomePage() {
    if(mounted) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/images/framework-logo.png',
                    height: 200,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:desafio_framework/models/todo.dart';
import 'package:desafio_framework/values/colors.dart';
import 'package:flutter/material.dart';

class TodoTile extends StatelessWidget {
  final Todo todo;

  const TodoTile({Key key, this.todo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        todo.completed
            ? Icons.check_circle_outline_outlined
            : Icons.radio_button_unchecked_outlined,
        color: Theme.of(context).primaryColor,
      ),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'TO-DO do usuário: ${todo.userId}',
            style: TextStyle(
              fontSize: 12,
              color: (Theme.of(context).brightness == Brightness.light) ? textColorLight : textColorDark,
            ),
          ),
          SizedBox(
            height: 4.0,
          ),
          Text(
            '${todo.title}',
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: (Theme.of(context).brightness == Brightness.light) ? titleColorLight : titleColorDark,
            ),
          ),
        ],
      ),
    );
  }
}

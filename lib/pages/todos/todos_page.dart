import 'package:desafio_framework/databases/database_todo.dart';
import 'package:desafio_framework/models/todo.dart';
import 'package:desafio_framework/pages/todos/components/todo_tile.dart';
import 'package:flutter/material.dart';

class TodosPage extends StatefulWidget {
  @override
  _TodosPageState createState() => _TodosPageState();
}

class _TodosPageState extends State<TodosPage> {
  List<Todo> todos = [];
  bool isLoading = true;

  @override
  void initState() {
    loadingTodos();
    super.initState();
  }

  /// Carrega as postagens no banco de dados local
  void loadingTodos() async {
    List<Todo> listTodos = await DatabaseTodo.db.getTodos();
    setState(() {
      todos = listTodos;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.black26,
      ),
      itemCount: todos.length,
      itemBuilder: (context, index) => TodoTile(
        todo: todos[index],
      ),

    );
  }
}

import 'package:flutter/material.dart';

const primaryColor = const Color.fromRGBO(95, 53, 113, 1);
const accentColor = const Color.fromRGBO(95, 53, 113, 1);
const textLightColor = Color(0xFF6A727D);
const secondaryColor = const Color.fromRGBO(148, 120, 159, 1);

const textColorLight = Color(0xFF757575);
const textColorDark = Color(0xFFE0E0E0);

const titleColorLight = Color(0xFF212121);
const titleColorDark = Color(0xFFF5F5F5);


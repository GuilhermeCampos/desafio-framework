import 'package:flutter/material.dart';
import 'package:desafio_framework/values/colors.dart' as colors;

final ThemeData themeDark = _buildThemeDark();

ThemeData _buildThemeDark() {
  final ThemeData base = ThemeData.dark();
  return base.copyWith(
      backgroundColor: Colors.grey[800],
      scaffoldBackgroundColor: Colors.grey[800],
      canvasColor: Colors.grey[800],
      dividerColor: Colors.grey[700],
      accentColor: colors.secondaryColor,
      primaryColor: colors.secondaryColor,
      accentIconTheme: _buildActionIconTheme(),
      appBarTheme: _buildAppBarTheme(),
      textSelectionTheme: _buildTextSelectionThemeData(),
      visualDensity: VisualDensity.adaptivePlatformDensity);
}

IconThemeData _buildActionIconTheme() {
  return IconThemeData(color: Colors.grey[400], size: 24);
}

AppBarTheme _buildAppBarTheme() {
  return AppBarTheme(
      iconTheme: _buildActionIconTheme(),
      brightness: Brightness.dark,
      color: Colors.grey[800]);
}

TextSelectionThemeData _buildTextSelectionThemeData() {
  return TextSelectionThemeData(
      cursorColor: colors.accentColor);
}

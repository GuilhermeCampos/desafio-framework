import 'package:flutter/material.dart';
import 'package:desafio_framework/values/colors.dart' as colors;

final ThemeData themeLight = _buildTheme();

ThemeData _buildTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
      backgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      canvasColor: Colors.white,
      accentColor: colors.accentColor,
      primaryColor: colors.primaryColor,
      dividerColor: Colors.grey[300],
      accentIconTheme: _buildActionIconTheme(),
      appBarTheme: _buildAppBarTheme(),
      textSelectionTheme: _buildTextSelectionThemeData(),
      visualDensity: VisualDensity.adaptivePlatformDensity);
}

IconThemeData _buildActionIconTheme() {
  return IconThemeData(color: Colors.grey[600], size: 24);
}

AppBarTheme _buildAppBarTheme() {
  return AppBarTheme(
      iconTheme: _buildActionIconTheme(),
      brightness: Brightness.light,
      color: Colors.white);
}

TextSelectionThemeData _buildTextSelectionThemeData() {
  return TextSelectionThemeData(cursorColor: colors.accentColor);
}
